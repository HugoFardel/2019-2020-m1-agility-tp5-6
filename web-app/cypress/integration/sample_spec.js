// Test 1 :
describe('test Configuration page', () => {
    it('good page', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()
    })
})


// Test 2 :
describe('test Count current segment', () => {
    it('28 segments detected', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()
        cy.get('.col-md-4 > .table > tbody > tr').should('have.length', 28)
    })
})



// Test 3 :
describe('test 0 vehicle should be find', () => {
    it('no vehicle found', () => {
        cy.visit('/')

        //verif graphique
        cy.get('.col-md-7 table tbody tr:first-child td:first-child').should("contain", "No vehicle available")

        //verif back (json)
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(!Object.keys(response.body).length)
        })

    })
})

// Test 4 :
describe('test modif vitesse segment ', () => {
    it('modification succeeded', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()
        cy.get(':nth-child(5) > :nth-child(2) > .form-control').clear().type(30)
        cy.get('#segment-5 > .btn').click()
        cy.get('.modal-title').contains('Update information')
        cy.get('.modal-footer > .btn').click()

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            expect(response.body['segments'][4]).to.have.property('speed',30)
        })
    })
})


// Test 5 :
describe('test modif roundabout ', () => {
    it('modification succeeded', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()

        cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').clear().type(4)
        cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').clear().type(15)
        cy.get(':nth-child(3) > .card-body > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            expect(response.body['crossroads'][2]).to.have.property('duration',15)
            expect(response.body['crossroads'][2]).to.have.property('capacity',4)
        })
    })
})


// Test 6 :
describe('test modif trafficlight ', () => {
    it('modification succeeded', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()

        //modif premier feu
        cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').clear().type(4)
        cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').clear().type(40)
        cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').clear().type(8)
        cy.get(':nth-child(1) > .card-body > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        cy.reload()

        //verif server (json)
        cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
        cy.get('@elements').should((response) => {
            expect(response.body['crossroads'][0]).to.have.property('orangeDuration',4)
            expect(response.body['crossroads'][0]).to.have.property('greenDuration',40)
            expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration',8)
        })
    })
})


// Test 7 :
describe('test add vehicle ', () => {
    it('add succeeded', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()

        //Ajout vehicle
        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(5)
            cy.get('input[name="destination"]').clear().type(26)
            cy.get('input[name="time"]').clear().type(50)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(19)
            cy.get('input[name="destination"]').clear().type(8)
            cy.get('input[name="time"]').clear().type(200)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(27)
            cy.get('input[name="destination"]').clear().type(2)
            cy.get('input[name="time"]').clear().type(150)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        //verif server
        cy.request('http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(Object.keys(response.body).length).eq(3)
            expect(response.body["200.0"]).to.have.length(1)
            expect(response.body["150.0"]).to.have.length(1)
            expect(response.body["50.0"]).to.have.length(1)
        })

        //verif graphique
        cy.get('.col-md-8 > .table > tbody > tr').should('have.length', 3)
        cy.get('.col-md-8 > .table > tbody > :nth-child(1) > :nth-child(2)').contains("200.0")
        cy.get('.col-md-8 > .table > tbody > :nth-child(2) > :nth-child(2)').contains("150.0")
        cy.get('.col-md-8 > .table > tbody > :nth-child(3) > :nth-child(2)').contains("50.0")
    })
})



// Test 8 :
describe('test simulation ', () => {
    it('simulation succeeded', () => {
        cy.visit('/#configuration')
        cy.get(':nth-child(2) > .nav-link').click()

        //Ajout vehicle
        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(5)
            cy.get('input[name="destination"]').clear().type(26)
            cy.get('input[name="time"]').clear().type(50)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(19)
            cy.get('input[name="destination"]').clear().type(8)
            cy.get('input[name="time"]').clear().type(200)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(27)
            cy.get('input[name="destination"]').clear().type(2)
            cy.get('input[name="time"]').clear().type(150)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.request('http://127.0.0.1:4567/vehicles').as('vehicles')

        cy.get('@vehicles').should((response) => {
            expect(response.body["200.0"][0]).to.have.property("speed", 0)
            expect(response.body["150.0"][0]).to.have.property("speed", 0)
            expect(response.body["50.0"][0]).to.have.property("speed", 0)
        })

        cy.get(':nth-child(1) > .nav-link').click()

        //Simulation
        cy.get('form[data-kind="run"').within(() => {
            cy.get('input[name="time"]').clear().type(120)
            cy.get('button').click()
        })

        cy.get('.progress-bar',{ timeout: 200000 }).should('have.attr','aria-valuenow','100')

        cy.get(':nth-child(1) > :nth-child(6) > div > span').contains("block")
        cy.get(':nth-child(2) > :nth-child(6) > div > span').contains("block")
        cy.get(':nth-child(3) > :nth-child(6) > div > span').contains("play_circle_filled")

    })
})


// Test 9 :
describe('test simulation vehicle', () => {
    it('simulation succeeded', () => {
        cy.visit('/')
        cy.reload()

        //verif front and back
        cy.get('.col-md-7 table tbody tr:first-child td:first-child').should("contain", "No vehicle available")
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(!Object.keys(response.body).length)
        })

        cy.get(':nth-child(1) > .nav-link').click()
        cy.get('.progress-bar').should('have.attr','aria-valuenow','0')

        cy.get(':nth-child(2) > .nav-link').click()

        //Ajout vehicles
        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(5)
            cy.get('input[name="destination"]').clear().type(26)
            cy.get('input[name="time"]').clear().type(50)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(19)
            cy.get('input[name="destination"]').clear().type(8)
            cy.get('input[name="time"]').clear().type(200)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(27)
            cy.get('input[name="destination"]').clear().type(2)
            cy.get('input[name="time"]').clear().type(150)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()


        cy.get(':nth-child(1) > .nav-link').click()

        //Simulation
        cy.get('form[data-kind="run"').within(() => {
            cy.get('input[name="time"]').clear().type(500)
            cy.get('button').click()
        })

        cy.get('.progress-bar',{ timeout: 500000 }).should('have.attr','aria-valuenow','100')

        cy.get(':nth-child(1) > :nth-child(6) > div > span').contains("block")
        cy.get(':nth-child(2) > :nth-child(6) > div > span').contains("block")
        cy.get(':nth-child(3) > :nth-child(6) > div > span').contains("block")


    })
})



// Test 10 :
describe('test simulation histoire 10', () => {
    it('simulation succeeded', () => {
        cy.visit('/')
        cy.reload()

        //verif front and back
        cy.get('.col-md-7 table tbody tr:first-child td:first-child').should("contain", "No vehicle available")
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
        cy.get('@vehicles').should((response) => {
            expect(!Object.keys(response.body).length)
        })

        cy.get(':nth-child(1) > .nav-link').click()
        cy.get('.progress-bar').should('have.attr','aria-valuenow','0')

        cy.get(':nth-child(2) > .nav-link').click()

        //Ajout vehicle
        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(5)
            cy.get('input[name="destination"]').clear().type(26)
            cy.get('input[name="time"]').clear().type(50)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(5)
            cy.get('input[name="destination"]').clear().type(26)
            cy.get('input[name="time"]').clear().type(80)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        cy.get('form[data-kind="vehicle"]').within(() => {
            cy.get('input[name="origin"]').clear().type(5)
            cy.get('input[name="destination"]').clear().type(26)
            cy.get('input[name="time"]').clear().type(80)
            cy.get('button').click()
        })
        cy.get('.modal-footer > .btn').click()

        //Simulation
        cy.get(':nth-child(1) > .nav-link').click()
        cy.get('form[data-kind="run"]').within(() => {
            cy.get('input[name="time"]').clear().type(200)
            cy.get('button').click()
        })

        cy.get('.progress-bar',{ timeout: 500000 }).should('have.attr','aria-valuenow','100')

        cy.get(':nth-child(1) > :nth-child(5)').contains("29")
        cy.get(':nth-child(2) > :nth-child(5)').contains("29")
        cy.get(':nth-child(3) > :nth-child(5)').contains("17")

    })
})